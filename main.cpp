#include <windows.h> // заголовочный файл, содержащий WINAPI
#include <iostream>
using namespace std;
// Прототип функции обработки сообщений с пользовательским названием:
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
HWND hMainWnd;
int CALLBACK KeyboardProc(int nCode, DWORD wParam, DWORD lParam)
{

 switch(wParam)
  {
  case WM_LBUTTONUP:
		cout << "1m\n";
		ShowWindow(hMainWnd, SW_HIDE);
	//	SetForegroundWindow(hMainWnd);
		break;
	case WM_RBUTTONUP:
		cout << "2m\n";
		ShowWindow(hMainWnd, SW_SHOW);
		break;
 // default:
//      return DefWindowProc(hWnd, uMsg, wParam, lParam);
  }

 return 0;
}
// Управляющая функция:
int WINAPI WinMain(HINSTANCE hInst, // дескриптор экземпляра приложения
                   HINSTANCE hPrevInst, // не используем
                   LPSTR lpCmdLine, // не используем
                   int nCmdShow) // режим отображения окошка
{
	RECT screen_rect;
	GetWindowRect(GetDesktopWindow(),&screen_rect); // разрешение экрана
	int sx = screen_rect.right/4;
	int sy = screen_rect.bottom/4;

	int x = screen_rect.right/2 - sx/2;
	int y = screen_rect.bottom/2 - sy/2;
	cout << "x " << x << "y " << "sx " << sx << "sy " << sy << endl;

	TCHAR szClassName[] = "WinClass"; // строка с именем класса
   // HWND hMainWnd; // создаём дескриптор будущего окошка
    MSG msg; // создём экземпляр структуры MSG для обработки сообщений
    WNDCLASSEX wc; // создаём экземпляр, для обращения к членам класса WNDCLASSEX
    wc.cbSize        = sizeof(wc); // размер структуры (в байтах)
    wc.style         = CS_HREDRAW | CS_VREDRAW; // стиль класса окошка
    wc.lpfnWndProc   = WndProc; // указатель на пользовательскую функцию
    wc.lpszMenuName  = NULL; // указатель на имя меню (у нас его нет)
    wc.lpszClassName = szClassName; // указатель на имя класса
    wc.cbWndExtra    = NULL; // число освобождаемых байтов в конце структуры
    wc.cbClsExtra    = NULL; // число освобождаемых байтов при создании экземпляра приложения
    wc.hIcon         = LoadIcon(NULL, IDI_WINLOGO); // декриптор пиктограммы
    wc.hIconSm       = LoadIcon(NULL, IDI_WINLOGO); // дескриптор маленькой пиктограммы (в трэе)
    wc.hCursor       = LoadCursor(NULL, IDC_ARROW); // дескриптор курсора
  //  wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH); // дескриптор кисти для закраски фона окна
    wc.hInstance     = hInst; // указатель на строку, содержащую имя меню, применяемого для класса
    if(!RegisterClassEx(&wc)){
        // в случае отсутствия регистрации класса:
        MessageBox(NULL, "Не получилось зарегистрировать класс!", "Ошибка", MB_OK);
        return NULL; // возвращаем, следовательно, выходим из WinMain
    }
    // Функция, создающая окошко:
    hMainWnd = CreateWindow(
        szClassName, // имя класса
        NULL,//"Полноценная оконная процедура", // имя окошка (то что сверху)
		WS_OVERLAPPEDWINDOW, // режимы отображения окошка
        x, // позиция окошка по оси х
        y, // позиция окошка по оси у (раз дефолт в х, то писать не нужно)
        sx, // ширина окошка
        sy, // высота окошка (раз дефолт в ширине, то писать не нужно)
        (HWND)NULL, // дескриптор родительского окна
        NULL, // дескриптор меню
        HINSTANCE(hInst), // дескриптор экземпляра приложения
        NULL); // ничего не передаём из WndProc
    if(!hMainWnd){
        // в случае некорректного создания окошка (неверные параметры и тп):
        MessageBox(NULL, "Не получилось создать окно!", "Ошибка", MB_OK);
        return NULL;
    }
    SetWindowsHookEx(WH_MOUSE_LL, (HOOKPROC)&KeyboardProc, GetModuleHandle(NULL), 0);
  //  ShowWindow(hMainWnd, nCmdShow); // отображаем окошко
    UpdateWindow(hMainWnd); // обновляем окошко
    while(GetMessage(&msg, NULL, NULL, NULL)){ // извлекаем сообщения из очереди, посылаемые фу-циями, ОС
        TranslateMessage(&msg); // интерпретируем сообщения
        DispatchMessage(&msg); // передаём сообщения обратно ОС
    }
    return msg.wParam; // возвращаем код выхода из приложения
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam){
    switch(uMsg){
    case WM_DESTROY: // если окошко закрылось, то:
        PostQuitMessage(NULL); // отправляем WinMain() сообщение WM_QUIT
        break;
    default:
        return DefWindowProc(hWnd, uMsg, wParam, lParam); // если закрыли окошко
    }
    return NULL; // возвращаем значение
}
